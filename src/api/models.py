from django.db import models

from django.contrib.auth.models import AbstractUser
from django.core.validators import MinLengthValidator


class User(AbstractUser):
    user_id = models.TextField(max_length=100, blank=False, null=False)

    def __str__(self):
        return 'User {}'.format(self.user_id)

    def __unicode__(self):
        return 'User Object {}'.format(self.id)

    @property
    def note_score(self):
        score = Note.objects.filter(user__user_id=self.user_id).aggregate(models.Sum('score'))
        return score['score__sum'] or 0


class Note(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='note_user')
    title = models.CharField(max_length=50, blank=False, null=False,
                             validators=[MinLengthValidator(3, 'Title is too short')])
    body = models.TextField(max_length=500, blank=False, null=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    score = models.IntegerField(default=0)

    def __str__(self):
        return 'Note {}'.format(self.id)

    def __unicode__(self):
        return 'Note Object {}'.format(self.id)
