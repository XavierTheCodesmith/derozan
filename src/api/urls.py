from .views import single_user, all_users, login_user, get_current_user, create_new_user, NoteView
from django.urls import path

urlpatterns = [
    path('users/single/<str:user_id>/', single_user, name='single_user'),
    path('users/all/', all_users, name='all_users'),
    path('auth/login/', login_user, name='login'),
    path('auth/register/', create_new_user, name='register'),
    path('auth/current_user/', get_current_user, name='current_user'),
    path('notes/all/', NoteView.as_view(), name='all_notes'),
    path('notes/single/<int:note_id>/', NoteView.as_view(), name='note'),
    path('notes/user/<str:user_id>/', NoteView.as_view(), name='user_notes'),
    path('notes/new/<str:user_id>/', NoteView.as_view(), name='new_note'),
]
