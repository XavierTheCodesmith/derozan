from .models import User, Note


def user_serializer(user_model: User):
    res = {
        'id': user_model.id,
        'user_id': user_model.user_id,
        'username': user_model.username,
        'email': user_model.email,
        'note_score': user_model.note_score
    }

    return res


def note_serializer(note_model: Note):
    res = {
        'id': note_model.id,
        'user': user_serializer(note_model.user),
        'title': note_model.title,
        'body': note_model.body,
        'created': note_model.created,
        'updated': note_model.updated,
        'score': note_model.score
    }

    return res
