from .models import User, Note
from django.http import JsonResponse, Http404
from django.views.generic import View
from django.contrib.auth import login, authenticate
from .serializers import user_serializer, note_serializer
import uuid


def single_user(request, user_id):
    user = User.objects.filter(user_id=user_id)[0]
    return JsonResponse({'data': user_serializer(user)})


def all_users(request):
    users = User.objects.all()
    return JsonResponse({'data': [user_serializer(u) for u in users]})


def login_user(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        return JsonResponse({'data': user_serializer(user)})
    return JsonResponse({'data': None}, status=400)


def get_current_user(request):
    if request.user.is_authenticated:
        user = request.user
        return JsonResponse({'data': user_serializer(user)})
    return JsonResponse({'data': None}, status=400)


def create_new_user(request):
    username = request.POST['username']
    password = request.POST['password']
    user_id = uuid.uuid4()

    new_user = User(username=username, user_id=user_id)
    new_user.set_password(password)
    new_user.save()

    return JsonResponse({'data': user_serializer(new_user)})


class NoteView(View):

    @staticmethod
    def get(request, user_id='', note_id=''):
        if user_id:
            notes = Note.objects.filter(user__user_id=user_id)
            return JsonResponse({'data': [note_serializer(n) for n in notes]})
        if note_id:
            note = Note.objects.filter(id=note_id)[0]
            return JsonResponse({'data': note_serializer(note)})
        all_notes = Note.objects.all()
        return JsonResponse({'data': [note_serializer(n) for n in all_notes]})

    @staticmethod
    def post(request, user_id=''):
        user = User.objects.filter(user_id=user_id).get()
        if user is not None:
            new_note = Note(title=request.POST.get('title'), body=request.POST.get('body'),
                            score=request.POST.get('score'), user=user)
            new_note.save()
            return JsonResponse({'data': note_serializer(new_note)})
        return Http404()
