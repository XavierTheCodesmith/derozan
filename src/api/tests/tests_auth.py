from django.test import TestCase, Client
from django.urls import reverse, resolve
from src.api.models import User
from src.api.views import login_user, get_current_user, create_new_user


class TestAuthClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.username = 'TestUser'
        cls.password = 'password'
        cls.user_id = 'dbdb37gvnienie999eu'
        cls.user = User(username=cls.username, user_id=cls.user_id)
        cls.user.set_password(cls.password)
        cls.user.save()

    def setUp(self) -> None:
        self.client = Client()

    def test_login_url(self):
        url = reverse('login')
        self.assertEqual(resolve(url).func, login_user)

    def test_login_view(self):
        req = self.client.post(reverse('login'), data={'username': self.username, 'password': self.password})
        self.assertIs(req.status_code, 200)
        res = req.json()
        data = res['data']
        self.assertIsNotNone(data)

    def test_get_current_user_url(self):
        url = reverse('current_user')
        self.assertEqual(resolve(url).func, get_current_user)

    def test_get_current_user_view(self):
        self.client.post(reverse('login'), data={'username': self.username, 'password': self.password})
        req = self.client.get(reverse('current_user'))
        self.assertIs(req.status_code, 200)
        res = req.json()
        data = res['data']
        self.assertIsNotNone(data)

    def test_create_new_user_url(self):
        url = reverse('register')
        self.assertEqual(resolve(url).func, create_new_user)

    def test_create_new_user_view(self):
        username = 'TestUser2'
        password = 'password123'
        req = self.client.post(reverse('register'), data={'username': username, 'password': password})
        self.assertIs(req.status_code, 200)
        user = User.objects.get(username=username)
        self.assertIsNotNone(user)

    def tearDownTestData(self):
        User.objects.filter(user_id=self.user_id).delete()
