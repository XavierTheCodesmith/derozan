from django.test import TestCase, Client
from django.urls import resolve, reverse
from src.api.views import all_users, single_user
from src.api.models import User


class TestUser(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.test_uuid = '465786jbwuqui1buig387bfbisubaiuqb'
        cls.test_pw = 'fake-password'
        cls.test_username = 'Fake_User'
        cls.test_user = User.objects.create_user(
            username=cls.test_username, password=cls.test_pw, user_id=cls.test_uuid)
        cls.test_user.save()

    def setUp(self) -> None:
        self.client = Client()

    def test_all_users_url(self):
        url = reverse('all_users')
        self.assertEquals(resolve(url).func, all_users)

    def test_single_user_url(self):
        url = reverse('single_user', args=['user_id'])
        self.assertEquals(resolve(url).func, single_user)

    def test_all_users_view(self):
        req = self.client.get(reverse('all_users'))
        self.assertEqual(req.status_code, 200)

    def test_single_user_view(self):
        req = self.client.get(reverse('single_user', kwargs={'user_id': self.test_uuid}))
        self.assertEqual(req.status_code, 200)

    def tearDownTestData(self):
        User.objects.filter(user_id=self.test_uuid).delete()
