from django.test import TestCase, Client
from src.api.models import Note, User
from django.urls import reverse


class TestNote(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.test_uid = '3g8fg8t3839279ifih'
        cls.test_user = User.objects.create_user(username='FakeUser', password='password', user_id=cls.test_uid)
        cls.test_user.save()

        new_note = Note(user=cls.test_user, title='Title 1', body='A body', score=3)
        new_note.save()

    def setUp(self) -> None:
        self.client = Client()

    def test_user_note_score(self):
        score = self.test_user.note_score
        self.assertEqual(score, 3)

    def test_note_view_all_notes(self):
        req = self.client.get(reverse('all_notes'))
        self.assertEqual(req.status_code, 200)
        res = req.json()
        data = res['data']
        self.assertEqual(len(data), 1)

    def test_note_view_single_note(self):
        req = self.client.get(reverse('note', kwargs={'note_id': 1}))
        self.assertEqual(req.status_code, 200)
        res = req.json()
        data = res['data']
        self.assertIsNotNone(data)
        self.assertEqual(data['id'], 1)
        self.assertEqual(data['title'], 'Title 1')
        self.assertEqual(data['body'], 'A body')

    def test_note_view_user_notes(self):
        req = self.client.get(reverse('user_notes', kwargs={'user_id': self.test_uid}))
        self.assertEqual(req.status_code, 200)
        res = req.json()
        data = res['data']
        self.assertIsNotNone(data)
        self.assertEqual(len(data), 1)

    def test_note_view_new_note_valid_user_id(self):
        req = self.client.post(reverse('new_note', kwargs={'user_id': self.test_uid}),
                               {'title': 'Title Two', 'body': 'Second body', 'score': 6})
        self.assertEqual(req.status_code, 200)
        notes = Note.objects.all()
        self.assertEqual(len(notes), 2)

    def tearDownTestData(self):
        Note.objects.delete()
        User.objects.filter(user_id=self.test_uid).delete()
